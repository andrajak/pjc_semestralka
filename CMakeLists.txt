cmake_minimum_required(VERSION 3.14)
project(MinimumSpanningTree LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)


add_executable(MinimumSpanningTree main.cpp Graph.h Graph.cpp Node.cpp Node.h Edge.cpp Edge.h MST.cpp MST.h Component.cpp Component.h Tests.cpp Tests.h)

if (CMAKE_CXX_COMPILER_ID MATCHES "Clang|AppleClang|GNU" )
    target_compile_options(MinimumSpanningTree PRIVATE -Wall -Wextra -Wunreachable-code -Wpedantic)
endif()
if (CMAKE_CXX_COMPILER_ID MATCHES "MSVC" )
    target_compile_options(MinimumSpanningTree PRIVATE /W4 )
endif()
