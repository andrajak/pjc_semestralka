#include "MST.h"

#include <iostream>
#include <algorithm>
#include <chrono>

namespace timer {
    auto now() {
        return std::chrono::high_resolution_clock::now();
    }

    template<typename TimePoint>
    long long to_ms(TimePoint tp) {
        return chrono::duration_cast<chrono::microseconds>(tp).count();
    }
}

void print_usage(string const& exe_name) {
    clog << "Usage: " << exe_name << " input_file " << endl;
    clog << " or  : " << exe_name << " input_file output_file" << endl;
    clog << endl;
    clog << "For valid format of input file see README.md please" << endl;
}

bool is_help(string const& argument) {
    return argument == "--help" || argument == "-h";
}

void printError(const string& file_name, const string& output, const string& message) {
    ofstream out(output, ios_base::app);
    if (out.is_open()) {
        out << "Input file: " << file_name << endl;
        out << message << endl << endl;
        out.close();
    } else
        clog << "Output file cannot be opened or created!" << endl;
}

void prepareErrorMessage(int argc, char** argv, const string& message) {
    if (argc == 3) {
        printError(argv[1], argv[2], message);
    }
    else
        clog << message << endl;
}

int prepareToEnd(ifstream& in, Graph* graph) {
    in.close();
    delete graph;
    return 0;
}

int main(int argc, char** argv) {

    // if you want use it check also "end" line of code in the bottom of main
    // auto start = timer::now();

    if (any_of(argv, argv+argc, is_help)) {
        print_usage(argv[0]);
        return 0;
    }

    if (argc == 1) {
        clog << "No input file given!" << endl;
        return 0;
    }

    if (argc > 3) {
        clog << "Too many arguments!" << endl;
        return 0;
    }

    for (int i = 1; i < argc; ++i) {
        if (argv[i][0] == '-') {
            clog << "Unknown argument/switch!" << endl;
            return 0;
        }
    }

    ifstream in(argv[1]);
    if (in.is_open()) {
        string source, destination;
        char left_bracket, right_bracket;
        int cost;
        Graph* graph = new Graph();

        while (in >> left_bracket) {
            if (left_bracket != '(') {
                prepareErrorMessage(argc, argv, "Some left bracket in given file is missing, please correct it!");
                return prepareToEnd(in, graph);
            };
            getline(in, source, ',');
            getline(in, destination, ',');
            if (source.empty() || destination.empty()) {
                prepareErrorMessage(argc, argv, "Some source or destination is missing name, please correct it!");
                return prepareToEnd(in, graph);
            }
            if (!(in >> cost) || cost < 1) {
                prepareErrorMessage(argc, argv, "Some cost in given file is invalid, please correct it!");
                return prepareToEnd(in, graph);
            }
            if (!(in >> right_bracket) || right_bracket != ')') {
                prepareErrorMessage(argc, argv, "Some right bracket in given file is missing, please correct it!");
                return prepareToEnd(in, graph);
            };

            graph->addEdge(source, destination, cost);
        }
        in.close();

        MST* mst = new MST();
        mst->calculateMST(graph);
        if (argc == 2)
            cout << mst->printMST();
        else
            mst->printMST(argv[1], argv[2]);
        delete graph;
        delete mst;

    } else {
        prepareErrorMessage(argc, argv, "Input file cannot be opened or doesn't exist!");
    }

    // second part of measuring time
    // auto end = timer::now();
    // cout << "Needed " << timer::to_ms(end - start) << " ms to finish." << endl;

    return 0;
}


