#include <iostream>
#include "Tests.h"

void Tests::test1() {
    auto *graph = new Graph();
    graph->addEdge("A", "B", 10);
    graph->addEdge("B", "A", 20);   // duplicate edge
    graph->addEdge("A", "B", 42);   // duplicate edge
    graph->addEdge("A", "C", 6);
    graph->addEdge("A", "D", 5);
    graph->addEdge("B", "D", 15);
    graph->addEdge("C", "D", 4);
    graph->addEdge("B", "D", 15);   // duplicate edge
    graph->addEdge("C", "D", 4);    // duplicate edge
    MST *mst = new MST();
    mst->calculateMST(graph);
    cout << mst->printMST();
    delete graph;
    delete mst;
}

void Tests::test2() {                                            // perfect data without duplicates
    Graph *graph = new Graph();                                  // MST is 370
    graph->addEdge("0", "1", 40);
    graph->addEdge("0", "7", 80);
    graph->addEdge("1", "7", 110);
    graph->addEdge("1", "2", 80);
    graph->addEdge("2", "3", 70);
    graph->addEdge("2", "5", 40);
    graph->addEdge("2", "8", 20);
    graph->addEdge("3", "4", 90);
    graph->addEdge("3", "5", 140);
    graph->addEdge("4", "5", 100);
    graph->addEdge("5", "6", 20);
    graph->addEdge("6", "8", 60);
    graph->addEdge("6", "7", 10);
    graph->addEdge("7", "8", 70);
    MST *mst = new MST();
    mst->calculateMST(graph);
    cout << mst->printMST();
    delete graph;
    delete mst;
}

void Tests::test3() {                                            // loop, duplicate edges, duplicate costs
    Graph *graph = new Graph();
    graph->addEdge("A", "A", 8);
    graph->addEdge("B", "C", 9);
    graph->addEdge("A", "C", 10);
    graph->addEdge("A", "B", 10);
    graph->addEdge("B", "C", 9);
    graph->addEdge("A", "C", 8);
    MST *mst = new MST();
    mst->calculateMST(graph);
    cout << mst->printMST();
    delete graph;
    delete mst;
}

void Tests::test4() {                                            // disconnected graph
    Graph *graph = new Graph();
    graph->addEdge("0", "1", 7);
    graph->addEdge("1", "2", 8);
    graph->addEdge("A", "B", 9);
    graph->addEdge("B", "C", 10);
    MST *mst = new MST();
    mst->calculateMST(graph);
    cout << mst->printMST();
    delete graph;
    delete mst;
}