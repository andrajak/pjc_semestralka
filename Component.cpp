#include "Component.h"

Component::Component(int rank, int parent) {
    this->rank = rank;
    this->parent = parent;
}

int Component::getRank() const {
    return rank;
}

void Component::setRank(int rank) {
    Component::rank = rank;
}

int Component::getParent() const {
    return parent;
}

void Component::setParent(int parent) {
    Component::parent = parent;
}

