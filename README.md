##### **Minimum Spanning Tree**
Nalezení minimální kostry grafu je realizováno pomocí upravené verze Borůvkova algoritmu. 
Algoritmus pracuje tak, že postupně spojuje komponenty souvislosti (na počátku je každý vrchol komponentou souvislosti) do větších a větších celků, až zůstane jen jediný, a to je hledaná minimální kostra. 
V každé fázi vybere pro každou komponentu souvislosti hranu s co nejnižší cenou, která směřuje do jiné komponenty souvislosti a tu přidá do kostry. 
V každé fázi se počet komponent souvislosti sníží nejméně dvakrát, počet fází bude tedy maximálně **logaritmus N (o základu 2)**, kde N je počet vrcholů grafu.

##### **Implementace**
Aplikace se skládá z těchto tříd: \
**Node** - má dvě proměnné, a to jméno (string) a id (int). Díky tomuto přístupu může program na vstupu jako jméno vrcholu očekávat libovolný textový řetězec a zároveň mít každý vrchol označný číslem (počínaje nulou a postupně inkrementujícím). Indexy kontejnerů v kombinaci s id vrcholů jsou potřebné pro tuto implementaci MST.\
**Edge** - má pointry na počátečný a cílový vrchol a cenu za tuto cestu (int)\
**Graph** - má set pointrů na vrcholy grafu. Jeho primární význam je v jednoduchém a rychlém zaručením, že žádný vrchol nebude do grafu přidán dvakrát, tedy že všechny vrcholy budou unikátní.\
**Component** - má dvě proměnné, a to rank (int) a parent (int). Pomocí těchto abstraktních komponent je z jednotlivých vrcholů postupně zbudovaná minimální kostra grafu.\
**MST** - hlavní třída aplikace, na vstupu příjmá graf a nalezne jeho minimální kostru a celkovou cenu. Dále poskytuje výpis těchto informací do konzole nebo do výstupního souboru.\
**Tests** - několik malých testů. Pro testování však prosím použijte přiložené soubory ve složce **input_files**.\
a souboru **main** - který se stará primárně o načtení dat z souboru, jejich parsování a validaci, výpis chybových hlášek a spuštění MST algoritmu.\
Tato upravená verze Borůvkova algoritmu povoluje smyčky na vrcholu, více hran mezi dvojicí vrcholů a duplicitní ceny hran.
Existence těchto hran algoritmus sice zpomalý, ale k správnému výsledku se dobere (kvůli duplicitním cenám hran může někdy existovat vícero minimálních koster, avšak se stejnou minimální cenou).

##### **Kompilace a špuštění**
`cd MinimumSpanningTree` \
`cmake .`\
`cmake --build ./`\
`./MinimumSpanningTree "input file"`\
nebo\
`./MinimumSpanningTree "input file" "output file"`

"input file" je povinný, jeho validní formát se dozvíte níže v tomto readme nebo pomocí přepínače --help\
"output file" není povinný, pokud ho nezadáte, tak se výstup programu vypíše do konzole

##### **Použití, vstupní data**
Jako první argument na vstupu program očekává soubor s hrany grafu, např:

(1,2,5)\
(2,A,7)\
(2,ABC,42)\
(ABC,1,8)

Každý řádek musí začínat a končit správnou závorkou a každá hodnota, tedy počáteční vrchol, cílový vrchol a cena, musí být oddělena čárkou.
I bílé znaky jsou brány jako součást textového řežezce, tedy např: "Hradec Králové" je validní název vrcholu.
Pozor ale, "HradecKrálové" nebo "hradec králové" by byly dva další, jiné vrcholy. \
Takovýto vstup je validní, vytvoří se graf s 4 vrcholy, a to (1,2,A,ABC), a 4 hrany. 
Cena jeho minimální kostry je 20.

##### **Měření**
Měření proběhlo vůči kódu v commitu `31dd9696`, na 4 jádrovém i5-6300HQ CPU taktovaném na 2.3 GHz, OS Linux Mint 19. 
Naimplementovaná je pouze jednovláknová varianta, a ta v Release potřebuje cca 387 až 466 mikrosekund k nalezení minimální kostry grafu o 9 vrcholech a 14 hranách.
