#pragma once

#include "Edge.h"
#include "Node.h"
#include "set"
#include "map"

using namespace std;

class Graph {
private:
    set<Node*, NodeComparator> unique_nodes;
    vector<Edge*> edges;

    const Node* addNode(const string& name);

public:
    Graph() = default;
    ~Graph();

    void addEdge(const string& source, const string& destination, int cost);

    const vector<Edge*> &getEdges() const;
    const set<Node*, NodeComparator> &getNodes() const;
};