#pragma once

#include <string>
#include "vector"

using namespace std;

class Node {
private:
    int id{};
    string name;

public:
    explicit Node(string name);
    ~Node() = default;

    friend bool operator< (const Node &lhs, const Node &rhs);

    const string &getName() const;

    int getId() const;
    void setId(int id);

};

struct NodeComparator
{
    bool operator()(const Node* lhs, const Node* rhs) const
    {
        return lhs->getName() < rhs->getName();
    }
};