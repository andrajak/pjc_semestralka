#include "MST.h"

// recursive path compression
int MST::find(vector<Component> &components, int i) {
    if (components.at(i).getParent() != i)
        components.at(i).setParent(find(components, components.at(i).getParent()));
    return components.at(i).getParent();
}

// union created by comparing ranks of component
void MST::createUnion(vector<Component> &components, int c1, int c2) {
    int c1_root, c2_root;
    c1_root = find(components, c1);
    c2_root = find(components, c2);

    // add lower rank component to root of high rank component
    if (components[c1_root].getRank() < components[c2_root].getRank())
        components[c1_root].setParent(c2_root);
    else if (components[c1_root].getRank() > components[c2_root].getRank())
        components[c2_root].setParent(c1_root);

    // else make root from one of them and increment its rank
    else {
        components[c2_root].setParent(c1_root);
        components[c1_root].setRank(components[c1_root].getRank()+1);
    }
}

void MST::calculateMST(Graph *graph) {

    vector<Component> components;
    int count_of_components = graph->getNodes().size();
    int MST_total_weight = 0;
    bool is_graph_disconnected = false;

    // prepare components .. in first step one node = one component
    components.reserve(count_of_components);
    for (int i = 0; i < count_of_components; ++i) {
        components.emplace_back(Component(0, i));
    }

    // helpful variable for check if graph is disconnected or not
    int disconnected_graph;

    ss << "Edges of MST are: " << endl;
    while (count_of_components > 1) {
        // set current count of components
        disconnected_graph = count_of_components;

        vector<int> cheapest_edge(graph->getNodes().size(),-42);

        for (int i = 0; i < (int)graph->getEdges().size(); ++i) {
            // find components (source/destination) of current edge
            int c_first = find(components, ((const Node*)graph->getEdges().at(i)->getSource())->getId());
            int c_second = find(components, ((const Node*)graph->getEdges().at(i)->getDestination())->getId());

            // check if current edge is shorter then previous edges of first and second component
            if (c_first != c_second) {
                if (cheapest_edge[c_first] == -42 ||
                graph->getEdges().at(cheapest_edge.at(c_first))->getCost() > graph->getEdges().at(i)->getCost())
                    cheapest_edge[c_first] = i;

                if (cheapest_edge[c_second] == -42 ||
                    graph->getEdges().at(cheapest_edge.at(c_second))->getCost() > graph->getEdges().at(i)->getCost())
                    cheapest_edge[c_second] = i;
            }
            // if nodes of current edge are from the same component, continue
            else
                continue;
        }
        // pick the cheapest edges and add/prepare them to output
        for (int i = 0; i < (int)graph->getNodes().size(); ++i) {
            // if cheapest edge for current component exists
            if (cheapest_edge[i] != -42) {
                int c_first = find(components, ((const Node*)graph->getEdges().at(cheapest_edge.at(i))->getSource())->getId());
                int c_second = find(components, ((const Node*)graph->getEdges().at(cheapest_edge.at(i))->getDestination())->getId());

                if (c_first != c_second) {
                    MST_total_weight += graph->getEdges().at(cheapest_edge.at(i))->getCost();

                    ss << "Edge (" << ((const Node*)graph->getEdges().at(cheapest_edge.at(i))->getSource())->getName()
                         << ","
                         << ((const Node*)graph->getEdges().at(cheapest_edge.at(i))->getDestination())->getName()
                         << ") "
                         << "cost "
                         << graph->getEdges().at(cheapest_edge.at(i))->getCost() << endl;

                    // if components are not same then do the union and decrement count of component
                    createUnion(components, c_first, c_second);
                    --count_of_components;
                }
                // if nodes of current edge are from the same component, then continue
                else
                    continue;
            }
        }
        // if count of components doesn't change during this step of algorithm, graph is disconnected
        if (disconnected_graph == count_of_components) {
            is_graph_disconnected = true;
            break;
        }
    }
    if (is_graph_disconnected) {
        ss.str("");
        ss << "Some edges are missing, graph is disconnected!" << endl;
    } else
        ss << "Total cost of MST is: " << MST_total_weight << endl;
}

// print to console
string MST::printMST() {
    return ss.str();
}

// print to file
void MST::printMST(const string& input_file, const string& output) {
    ofstream out(output, ios_base::app);
    if (out.is_open()) {
        out << "Input file: " << input_file << endl;
        out << ss.str() << endl;
        out.close();
    } else
        cout << "Output file cannot be opened or created!" << endl;
}
