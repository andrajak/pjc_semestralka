#pragma once

#include "Graph.h"
#include "Component.h"
#include <sstream>
#include <iostream>
#include <fstream>

using namespace std;

class MST {
private:
    stringstream ss;

    int find(vector<Component>& components, int i);
    void createUnion(vector<Component>& components, int c1, int c2);

public:
    MST() = default;
    ~MST() = default;

    void calculateMST(Graph* graph);
    string printMST();
    void printMST(const string& input_file, const string& output);
};
