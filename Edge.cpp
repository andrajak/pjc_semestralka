#include "Edge.h"

Edge::Edge(const Node* source, const Node* destination, int cost) {
    this->source = source;
    this->destination = destination;
    this->cost = cost;
}

Edge::Edge(const Edge &e) : source(e.source), destination(e.destination), cost(e.cost) {}

Edge::~Edge() {
    source = nullptr;
    destination = nullptr;
}

bool operator<(const Edge &lhs, const Edge &rhs) {
    return lhs.source->getName() < rhs.source->getName() ||
            (!(rhs.source->getName() < lhs.source->getName()) && lhs.destination->getName() < rhs.destination->getName());
}

int Edge::getCost() const {
    return cost;
}

void Edge::setCost(int cost) {
    Edge::cost = cost;
}

const Node* Edge::getSource() const {
    return source;
}

const Node* Edge::getDestination() const {
    return destination;
}

void Edge::setSource(const Node *source) {
    Edge::source = source;
}

void Edge::setDestination(const Node *destination) {
    Edge::destination = destination;
}









