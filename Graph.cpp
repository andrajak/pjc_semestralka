#include "Graph.h"

Graph::~Graph() {
    for (auto & edge : edges) {
        delete edge;
    }
    for (auto & node : unique_nodes) {
        delete node;
    }
}

const Node* Graph::addNode(const string& name) {
    Node* n = new Node(name);
    if (this->unique_nodes.count(n)) {
        auto it = unique_nodes.find(n);
        delete n;
        return (*it);
    } else {
        n->setId(this->unique_nodes.size());
        this->unique_nodes.emplace(n);
        return n;
    }

}

void Graph::addEdge(const string& source, const string& destination, int cost) {
    const Node *s = addNode(source);
    const Node *d = addNode(destination);

    Edge* e = new Edge();
    e->setSource(s);
    e->setDestination(d);
    e->setCost(cost);

    this->edges.push_back(e);
}

const vector<Edge*> &Graph::getEdges() const {
    return edges;
}

const set<Node*, NodeComparator> &Graph::getNodes() const {
    return unique_nodes;
}


