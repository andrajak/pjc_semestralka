#pragma once

class Component {
private:
    int rank;
    int parent;

public:
    Component() = default;
    Component(int rank, int parent);
    ~Component() = default;

    int getRank() const;
    void setRank(int rank);

    int getParent() const;
    void setParent(int parent);
};



