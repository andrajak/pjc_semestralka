#pragma once

#include "Node.h"
#include <string>
#include "vector"

using namespace std;

class Edge {
private:
    const Node* source;
    const Node* destination;
    int cost;

public:
    Edge() = default;
    Edge(const Node* source, const Node* destination, int weight);
    Edge(const Edge& e);
    ~Edge();

    friend bool operator< (const Edge &left, const Edge &right);

    int getCost() const;
    void setCost(int cost);

    const Node* getSource() const;
    void setSource(const Node *source);

    const Node* getDestination() const;
    void setDestination(const Node *destination);
};
