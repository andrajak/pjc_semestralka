#include "Node.h"

Node::Node(string name) {
    this->name = name;
}

const string &Node::getName() const {
    return name;
}

int Node::getId() const {
    return id;
}

void Node::setId(int id) {
    this->id = id;
}

bool operator<(const Node &lhs, const Node &rhs) {
    return lhs.name < rhs.name;
}



